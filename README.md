A project that uses an input filter "template", a second input filter for template "values", and outputs a generated filter.


Intended use:

- You create multiple filters. The filter names must start with "__IN\_". The filter contains multiple rules.
- Each rule NAME may contain multiple labels. The syntax to declare labels is:
  "free text" "// "KEY1:VALUE1, KEY2:VALUE2, etc."
  Here's an example rule name:
  ```RELIC - Mage suffixes // SLOT:RELIC, AFFIX:PREFIX, CLASS:MAGE, SPEC:RUNEMASTER, BUILD:LIGHTNING```
  You can have as many or as few labels as you want, and you are free to choose whatever names you want for them.
  This example rule contains two conditions:
- - EquipmentType: with a few relic subtype selections
- - Affix: with a selection of suffixes for the relic.

- You create a "special filter" called "__TEMPLATE". This exact name is matched, so if you want to have multiple templates, just rename them around.
- The __TEMPLATE filter is parsed according to rules that will be explained shortly. After processing, a filter called "__GENERATED" is created
  _(only if another filter called "__GENERATED" doesn't already exist)_

- The "__GENERATED" filter is created as follows:
- All rules from the "__TEMPLATE" are copied as-they-are.
- FilterRules whose names contain "//" contain instructions to be applied while copying. These operations make use of the LABELS described above.
- - The "SELECT Label1,Label2,etc." instruction will copy the original rule multiple times: once for every selected rule in the "__IN\_" filters. 
    Example:
    ```
        Filter name: __IN_MAGE_AFFIXES_PER_ARMOR:
        Rule #1: Relic - prefixes // SLOT:RELIC, AFFIX:PREFIX, CLASS:MAGE, SPEC:RUNEMASTER, BUILD:LIGHTNING
        Rule #2: Relic - suffixes // SLOT:RELIC, AFFIX:SUFFIX, CLASS:MAGE, SPEC:RUNEMASTER, BUILD:LIGHTNING
        Rule #3: Ring  - prefixes // SLOT:RING,  AFFIX:PREFIX, CLASS:MAGE, SPEC:RUNEMASTER, BUILD:LIGHTNING
    
        Filter name: __TEMPLATE:
        Item with 2+ good affixes // SELECT(SLOT:RELIC,SPEC:RUNEMASTER,BUILD:LIGHTNING)
    ```
    In this case, 2 rules match the selection labels (all labels must match): Rules #1 and #2.
    The Rule from the __TEMPLATE will then be duplicated twice. The first copy will include the conditions in Rule#1. The second copy will contain the conditions in Rule #2.
    Currently, the conditions are copied as they are, except for affix conditions, for which only the list of affixes will be copied from the affix condition.
    This means you can have different rules in the __TEMPLATE which SELECT the same labels, but you can configure for 1/2/3/4 affixes in each.
    The rule definition in the __TEMPLATE should already contain an "Affix" condition with the desired number of minimum affixes.


- - The "JOIN_BY Label1,Label2, etc" instruction will merge conditions in all the rules that match the filter.
    Example:
    ```
    Good equipment // SELECT SPEC:RUNEMASTER, BUILD:LIGHTNING; JOIN_BY SLOT
    ```
    Will create two rules:
    Good RELIC: which will have two conditions. The conditions will be automatically joined by type:
        so the relic subtype lists will be joined into one 
        and the affix conditions will be joined into a single affix condition, joining the affixes in both lists.
    You can even externalize the relic subtypes in a third rule, and the join will be \*chefs kiss\*.

    All labels used in the JOIN_BY condition can be used as wildcards in the rule name.
    If you use the label key in the rule name, it will be replaced with the label value.
    Here's an example with $EQUIPMENT and $AFFIX in the __TEMPLATE rule names.
    ```
        $SLOT with 2+ good $AFFIX // SELECT(SLOT:RELIC, AFFIX:PREFIX SPEC:RUNEMASTER,BUILD:LIGHTNING) JOIN_BY SLOT, AFFIX
    ```
    Will become:
    ```
        RELIC with 2+ good PREFIX
    ```
