In order to get a LastEpoch database:
1. Go to https://www.lastepochtools.com/data/version100/i18n/full/en.json in your browser and save this file as ~/Downloads/tooltip-keys.json
2. Run in the terminal:
```
cat ~/Downloads/tooltip-keys.json | jq '.' > ~/Downloads/tooltip-keys-formatted.json
```
3. Go to https://www.lastepochtools.com/db/prefixes
4. Press F12 to open developer console
5. console.log(JSON.stringify(window.itemDB.affixList));
6. Expand it, right-click the message (without clicking it - that would cause it collapse again.)
   Copy it, save it to a file: ~/Downloads/affix-list.json
   Remove the line that says "debugger eval code:1:9"
7. Run in the terminal
```
jq '.' ~/Downloads/affix-list.json > ~/Downloads/affixList-formatted.json
jq '.singleAffixes
        | to_entries
        | map({
                affixId: .value.affixId,
                group: .value.group,
                type: .value.type,
                classSpecificity: .value.classSpecificity,
                rerollChance: .value.rerollChance
          })
        | sort_by(.rerollChance)
        | reverse' \
    ~/Downloads/affix-list.json \
    > ~/Downloads/singleAffixes-parsed.json
jq '.multiAffixes
        | to_entries
        | map({
                affixId: .value.affixId,
                group: .value.group,
                type: .value.type,
                classSpecificity: .value.classSpecificity,
                rerollChance: .value.rerollChance
          })
        | sort_by(.rerollChance)
        | reverse' \
    ~/Downloads/affix-list.json \
    > ~/Downloads/multiAffixes-parsed.json


Affix Type 0 = prefix
Affix Type 1 = suffix
Affix Group 0 = Idol Affix
Affix Group 1 = Equipment Affix
