package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.ItemFilter;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ItemFilterMarshaller {

    Unmarshaller unmarshaller;
    Marshaller marshaller;

    public ItemFilterMarshaller() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(ItemFilter.class);
        this.marshaller = context.createMarshaller();
        this.marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        this.marshaller.setProperty("org.glassfish.jaxb.indentString", "  ");
        this.marshaller.setProperty("org.glassfish.jaxb.xmlDeclaration", false);
        this.marshaller.setProperty("org.glassfish.jaxb.namespacePrefixMapper", new CustomNamespacePrefixMapper());

        this.unmarshaller = context.createUnmarshaller();
    }

    public void marshall(File file, ItemFilter itemFilter) throws JAXBException, TransformerException {
        this.marshaller.marshal(itemFilter, file);
    }

    public ItemFilter unmarshall(File file) throws FileNotFoundException, JAXBException {
        return (ItemFilter) this.unmarshaller.unmarshal(new FileInputStream(file));
    }

}
