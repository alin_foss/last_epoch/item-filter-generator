package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.RuleType;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class RuleTypeAdapter extends XmlAdapter<String, RuleType> {

    @Override
    public RuleType unmarshal(String ruleTypeName) {
        return RuleType.getFromName(ruleTypeName);
    }

    @Override
    public String marshal(RuleType ruleType) {
        return ruleType.name;
    }

}
