package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.RuleRecolorColor;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class RuleRecolorColorAdapter extends XmlAdapter<Integer, RuleRecolorColor> {

    @Override
    public RuleRecolorColor unmarshal(Integer ruleRecolorColorID) {
        return RuleRecolorColor.getFromCode(ruleRecolorColorID);
    }

    @Override
    public Integer marshal(RuleRecolorColor ruleRecolorColor) {
        return ruleRecolorColor.intCode;
    }

}
