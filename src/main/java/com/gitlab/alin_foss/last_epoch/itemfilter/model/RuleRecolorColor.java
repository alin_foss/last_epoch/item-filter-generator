package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum RuleRecolorColor {
    ERROR(-1)

    ,WHITE(0)
    ,GRAY(1)
    ,LIME(2)
    ,YELLOW(3)
    ,LIGHT_ORANGE(4)
    ,ORANGE(5)
    ,DARK_ORANGE(6)
    ,RED(7)
    ,LIGHT_PINK(8)
    ,PINK(9)
    ,PURPLE(10)
    ,LIGHT_PURPLE(11)
    ,BLUE(12)
    ,LIGHT_BLUE(13)
    ,CYAN(14)
    ,AQUA(15)
    ,GREEN(16)
    ,DARK_GREEN(17)
    ;

    @XmlValue
    public final Integer intCode;

    private static final Map<Integer, RuleRecolorColor> BY_INTCODE = new HashMap<>();

    static {
        for (RuleRecolorColor fi: values()) {
            BY_INTCODE.put(fi.intCode, fi);
        }
    }

    public static RuleRecolorColor getFromCode(Integer intCode) {
        return BY_INTCODE.get(intCode);
    }

}
