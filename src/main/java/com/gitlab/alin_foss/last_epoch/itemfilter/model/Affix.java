package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import com.gitlab.alin_foss.last_epoch.db.Affixes;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.util.Arrays;
import java.util.List;


@Getter
@Setter
@Accessors(chain = true)
@RequiredArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class Affix {

    // TODO singleSetAffix;
    Integer affixId;
    // TODO levelRequirement;
    AffixType type;
    // TODO Double standardAffixEffectModifier;
    Double rerollChance;
    // TODO Group. Idol/Equipment? 1 = idol / 2 = equipment?
    List<EquipmentType> canRollOn;
    // TODO Map<EquipmentType, Double> specificRerollChances;
    // TODO rollsOn ?!!
    // TODO classSpecificity
    // TODO tiers
    // TODO displayCategory
    // TODO titleType
    // TODO t6compatibility
    // TODO useGeneratedName
    // TODO specialAffixType
    // TODO affixMorphology
    // TODO lootFilterOverrideNameKey
    // TODO affixDisplayNameKey
    // TODO affixTitleKey
    // TODO affixProperties
    // TODO rarityTier
    // TODO isSingle
    // TODO rarity
    // TODO id
    // TODO ea

    public Affix(Integer affixId) {
        Affix actualAffix = Affixes.getFromId(affixId);
        this.affixId = affixId;
        this.type = actualAffix.type;
        this.rerollChance = actualAffix.rerollChance;
        this.canRollOn = actualAffix.canRollOn;
    }

    public Affix(
            Integer affixId,
            AffixType type,
            Double rerollChance,
            List<EquipmentType> canRollOn
    ) {
        this.affixId = affixId;
        this.type = type;
        this.rerollChance = rerollChance;
        this.canRollOn = canRollOn;
        Affixes.registerAffix(this);
    }

    public Affix(
            Integer affixId,
            AffixType type,
            Double rerollChance,
            EquipmentType... canRollOn
    ) {
        this(
                affixId,
                type,
                rerollChance,
                Arrays.stream(canRollOn).toList()
        );
    }

}
