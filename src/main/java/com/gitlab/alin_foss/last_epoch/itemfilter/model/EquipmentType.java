package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum EquipmentType {
    ERROR("ERROR", false, false)

    ,ONE_HANDED_DAGGER("ONE_HANDED_DAGGER", true, false)
    ,ONE_HANDED_SWORD("ONE_HANDED_SWORD", true, false)
    ,ONE_HANDED_SCEPTRE("ONE_HANDED_SCEPTRE", true, false)
    ,ONE_HANDED_MACES("ONE_HANDED_MACES", true, false)
    ,ONE_HANDED_AXE("ONE_HANDED_AXE", true, false)
    ,WAND("WAND", true, false)

    ,TWO_HANDED_AXE("TWO_HANDED_AXE", true, false)
    ,TWO_HANDED_MACE("TWO_HANDED_MACE", true, false)
    ,TWO_HANDED_SPEAR("TWO_HANDED_SPEAR", true, false)
    ,TWO_HANDED_STAFF("TWO_HANDED_STAFF", true, false)
    ,TWO_HANDED_SWORD("TWO_HANDED_SWORD", true, false)
    ,BOW("BOW", true, false)

    ,CATALYST("CATALYST", true, false)
    ,QUIVER("QUIVER", true, false)
    ,SHIELD("SHIELD", true, false)

    ,HELMET("HELMET", false, false)
    ,BODY_ARMOR("BODY_ARMOR", false, false)
    ,BELT("BELT", false, false)
    ,BOOTS("BOOTS", false, false)
    ,GLOVES("GLOVES", false, false)

    ,AMULET("AMULET", false, false)
    ,RING("RING", false, false)
    ,RELIC("RELIC", false, false)

    ,IDOL_1X1_ETERRA("IDOL_1x1_ETERRA", false, true)
    ,IDOL_1X1_LAGON("IDOL_1x1_LAGON", false, true)
    ,IDOL_2X1("IDOL_2x1", false, true)
    ,IDOL_1X2("IDOL_1x2", false, true)
    ,IDOL_3X1("IDOL_3x1", false, true)
    ,IDOL_1X3("IDOL_1x3", false, true)
    ,IDOL_4X1("IDOL_4x1", false, true)
    ,IDOL_1X4("IDOL_1x4", false, true)
    ,IDOL_2X2("IDOL_2x2", false, true)
    ;

    @XmlValue
    public final String name;

    @XmlTransient
    public final Boolean isWeapon;

    @XmlTransient
    public final Boolean isIdol;

    private static final Map<String, EquipmentType> BY_NAME = new HashMap<>();

    static {
        for (EquipmentType fi: values()) {
            BY_NAME.put(fi.name, fi);
        }
    }

    public static EquipmentType getFromName(String name) {
        return BY_NAME.get(name);
    }

    public List<EquipmentSubType> getSubtypes() {
        return EquipmentSubType.of(this);
    }

}
