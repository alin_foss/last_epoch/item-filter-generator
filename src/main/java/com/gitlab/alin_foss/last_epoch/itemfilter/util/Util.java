package com.gitlab.alin_foss.last_epoch.itemfilter.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Util {

    public static File getResourceFile(String resourceName) throws FileNotFoundException {
        URL resource = Util.class.getClassLoader().getResource(resourceName);
        if(resource == null) {
            throw new FileNotFoundException(resourceName);
        }
        String filePath = resource.getFile();
        return new File(filePath);
    }

}
