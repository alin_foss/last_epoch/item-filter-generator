package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.EquipmentType;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class EquipmentTypeAdapter extends XmlAdapter<String, EquipmentType> {

    @Override
    public EquipmentType unmarshal(String conditionTypeName) {
        return EquipmentType.getFromName(conditionTypeName);
    }

    @Override
    public String marshal(EquipmentType equipmentType) {
        return equipmentType.name;
    }

}
