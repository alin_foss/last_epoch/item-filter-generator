package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.ConditionType;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class ConditionTypeAdapter extends XmlAdapter<String, ConditionType> {

    @Override
    public ConditionType unmarshal(String conditionTypeName) {
        return ConditionType.getFromName(conditionTypeName);
    }

    @Override
    public String marshal(ConditionType conditionType) {
        return conditionType.name;
    }

}
