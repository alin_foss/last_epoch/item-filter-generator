package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters.AffixAdapter;
import com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters.ConditionTypeAdapter;
import com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters.EquipmentTypeAdapter;
import jakarta.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

import static com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.CustomNamespacePrefixMapper.NAMESPACE_URL;
import static com.gitlab.alin_foss.last_epoch.itemfilter.model.ConditionType.RARITY;
import static com.gitlab.alin_foss.last_epoch.itemfilter.model.ConditionType.SUBTYPE;


@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
@RequiredArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Condition implements Cloneable {

    @XmlAttribute(name = "type", namespace = NAMESPACE_URL)
    @XmlJavaTypeAdapter(ConditionTypeAdapter.class)
    ConditionType itype;

    // =========================
    // Rarity condition fields ||
    // =========================
    String rarity;


    // =========================
    // Affix condition fields ||
    // =========================
    @XmlElementWrapper
    @XmlElement(name="int")
    @XmlJavaTypeAdapter(AffixAdapter.class)
    List<Affix> affixes;
    String comparsion;
    Integer comparsionValue;
    Integer minOnTheSameItem;
    String combinedComparsion;
    Integer combinedComparsionValue;
    Boolean advanced;


    // ===========================
    // Subtype condition fields ||
    // ===========================
    @XmlElementWrapper
    @XmlElement(name="EquipmentType")
    @XmlJavaTypeAdapter(EquipmentTypeAdapter.class)
    List<EquipmentType> type;
    @XmlElementWrapper
    @XmlElement(name="int")
    List<Integer> subTypes; // TODO switch to the weapon subtype classes/enums maybe

    public Condition(Condition clone) {
        this.itype = clone.itype;
        this.rarity = clone.rarity;
        this.affixes = clone.affixes;
        this.comparsion = clone.comparsion;
        this.comparsionValue = clone.comparsionValue;
        this.minOnTheSameItem = clone.minOnTheSameItem;
        this.combinedComparsion = clone.combinedComparsion;
        this.combinedComparsionValue = clone.combinedComparsionValue;
        this.advanced = clone.advanced;
        this.type = clone.type == null ? null : new ArrayList<>(clone.type);
        this.subTypes = clone.subTypes == null ? null : new ArrayList<>(clone.subTypes);
    }

    public static Condition newRarityCondition(
            boolean isNormal,
            boolean isMagic,
            boolean isRare,
            boolean isExalted,
            boolean isUnique,
            boolean isSet
    ) {
        StringBuilder sb = new StringBuilder();
        if(isNormal) sb.append("NORMAL ");
        if(isMagic) sb.append("MAGIC ");
        if(isRare) sb.append("RARE ");
        if(isUnique) sb.append("UNIQUE ");
        if(isSet) sb.append("SET ");
        if(isExalted) sb.append("EXALTED ");

        if(sb.isEmpty()) {
            sb.append("NONE");
        } else {
            sb.setLength(sb.length() - 1);
        }

        Condition condition = new Condition();
        condition.itype = RARITY;
        condition.rarity = sb.toString();
        return condition;
    }

    public static Condition newAffixCondition(
            List<Affix> affixes,
            Integer minOnTheSameItem
    ) {
        Condition condition = new Condition();
        condition.itype = ConditionType.AFFIX;
        condition.affixes = affixes;
        condition.comparsion = "ANY";
        condition.comparsionValue = 0;
        condition.minOnTheSameItem = minOnTheSameItem;
        condition.combinedComparsion = "ANY";
        condition.combinedComparsionValue = 1;
        condition.advanced = false;
        return  condition;
    }

    public static Condition newSubtypeCondition(
            List<EquipmentType> type,
            List<Integer> subTypes
    ) {
        Condition condition = new Condition();
        condition.itype = SUBTYPE;
        condition.type = type;
        condition.subTypes = subTypes;
        return condition;
    }

    public void addAllTypes(List<EquipmentType> types) {
        this.type.addAll(types);
    }

    public void addAllSubtypes(List<Integer> subTypes) {
        this.subTypes.addAll(subTypes);
    }
}
