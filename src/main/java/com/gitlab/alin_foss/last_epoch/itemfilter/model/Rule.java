package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters.RuleRecolorColorAdapter;
import com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters.RuleTypeAdapter;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
@RequiredArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class Rule {

    @XmlJavaTypeAdapter(RuleTypeAdapter.class)
    RuleType type = RuleType.SHOW;

    @XmlElementWrapper
    @XmlElement(name = "Condition")
    final List<Condition> conditions = new ArrayList<>();

    @XmlJavaTypeAdapter(RuleRecolorColorAdapter.class)
    RuleRecolorColor color = RuleRecolorColor.WHITE;

    Boolean isEnabled = true;

    Boolean levelDependent = false;
    Integer minLvl = 0;
    Integer maxLvl = 0;

    Boolean emphasized = false;

    String nameOverride = "";



    public Rule(Rule clone) {
        this.type = clone.getType();
        this.conditions.addAll(clone.conditions.stream().map(x -> new Condition(x)).toList());
        this.color = clone.color;
        this.isEnabled = clone.isEnabled;
        this.levelDependent = clone.levelDependent;
        this.minLvl = clone.minLvl;
        this.maxLvl = clone.maxLvl;
        this.emphasized = clone.emphasized;
        this.nameOverride = clone.nameOverride;
    }

    public List<Condition> getConditions() {
        return Collections.unmodifiableList(this.conditions);
    }

    public Rule addCondition(Condition rule) {
        this.conditions.add(rule);
        return this;
    }
    public Rule addAllConditions(Collection<Condition> rules) {
        this.conditions.addAll(rules);
        return this;
    }

    public Rule clearConditions() {
        this.conditions.clear();
        return this;
    }

}
