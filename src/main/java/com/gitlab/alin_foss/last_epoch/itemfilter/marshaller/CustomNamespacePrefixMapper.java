package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller;

import org.glassfish.jaxb.runtime.marshaller.NamespacePrefixMapper;

public class CustomNamespacePrefixMapper extends NamespacePrefixMapper {

    public static final String NAMESPACE_URL="http://www.w3.org/2001/XMLSchema-instance";
    public static final String NAMESPACE_PREFIX="i";

    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        if(NAMESPACE_URL.equals(namespaceUri)) {
            return NAMESPACE_PREFIX;
        }
        return suggestion;
    }

}
