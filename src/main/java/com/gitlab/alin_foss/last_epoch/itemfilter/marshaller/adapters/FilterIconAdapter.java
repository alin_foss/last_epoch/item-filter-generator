package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.FilterIcon;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class FilterIconAdapter extends XmlAdapter<Integer, FilterIcon> {

    @Override
    public FilterIcon unmarshal(Integer iconID) {
        return FilterIcon.getFromCode(iconID);
    }

    @Override
    public Integer marshal(FilterIcon filterIcon) {
        return filterIcon.intCode;
    }

}
