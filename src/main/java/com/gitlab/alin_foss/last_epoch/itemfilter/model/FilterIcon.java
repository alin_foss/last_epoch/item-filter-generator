package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum FilterIcon {
    ERROR(-1)
    ,NONE(0)     ,SKULL(1)        ,FIRE(2)         ,SIGIL(3)    ,KNIVES(4)   ,HELMET(5)
    ,SWORD(6)    ,AXE(7)          ,AMULET(8)       ,ARMOR(9)    ,BELT(10)    ,SHOES(11)
    ,GLOVES(12)  ,HELMET2(13)     ,CYCLOPENIS(14)  ,RING(15)    ,SHIELD(16)  ,RUNE(17)
    ,SQUARE(18)  ,HEX(19)         ,TOTEMS(20)      ,ANVIL(21)   ,KEY(22)     ,STAR(23)
    ;

    public final Integer intCode;

    private static final Map<Integer,  FilterIcon> BY_INTCODE = new HashMap<>();

    static {
        for (FilterIcon fi: values()) {
            BY_INTCODE.put(fi.intCode, fi);
        }
    }

    public static FilterIcon getFromCode(Integer intCode) {
        return BY_INTCODE.get(intCode);
    }

}
