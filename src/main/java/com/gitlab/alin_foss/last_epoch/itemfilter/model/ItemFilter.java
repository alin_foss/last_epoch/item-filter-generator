package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters.FilterIconAdapter;
import com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters.FilterIconColorAdapter;
import jakarta.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
@RequiredArgsConstructor
@XmlRootElement(name="ItemFilter")
@XmlAccessorType(XmlAccessType.FIELD)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ItemFilter {

    String name;


    @XmlJavaTypeAdapter(FilterIconAdapter.class)
    FilterIcon filterIcon;
    @XmlJavaTypeAdapter(FilterIconColorAdapter.class)
    FilterIconColor filterIconColor;
    String description;

    String lastModifiedInVersion = "1.0.0.4";
    Integer lootFilterVersion = 2;

    @XmlElementWrapper
    @XmlElement(name="Rule")
    final List<Rule> rules = new ArrayList<>();

    public ItemFilter(ItemFilter clone) {
        this.name = clone.name;
        this.filterIcon = clone.filterIcon;
        this.filterIconColor = clone.filterIconColor;
        this.description = clone.description;
        this.lastModifiedInVersion = clone.lastModifiedInVersion;
        this.lootFilterVersion = clone.lootFilterVersion;
        this.rules.addAll(clone.rules);
    }

    public List<Rule> getRules() {
        return Collections.unmodifiableList(this.rules);
    }

    public ItemFilter addRule(Rule rule) {
        this.rules.add(rule);
        return this;
    }
    public ItemFilter addAllRules(Collection<Rule> rules) {
        this.rules.addAll(rules);
        return this;
    }

    public ItemFilter clearRules() {
        this.rules.clear();
        return this;
    }

}
