package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters;

import com.gitlab.alin_foss.last_epoch.db.Affixes;
import com.gitlab.alin_foss.last_epoch.itemfilter.model.Affix;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class AffixAdapter extends XmlAdapter<Integer, Affix> {

    @Override
    public Affix unmarshal(Integer affixId) {
        return Affixes.getFromId(affixId);
    }

    @Override
    public Integer marshal(Affix affix) {
        return affix.getAffixId();
    }

}
