package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import jakarta.xml.bind.annotation.XmlValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ConditionType {
    ERROR("ERROR")

    ,AFFIX("AffixCondition")
    ,SUBTYPE("SubTypeCondition")
    ,RARITY("RarityCondition")
    ;

    @XmlValue
    public final String name;

    private static final Map<String, ConditionType> BY_NAME = new HashMap<>();

    static {
        for (ConditionType fi: values()) {
            BY_NAME.put(fi.name, fi);
        }
    }

    public static ConditionType getFromName(String name) {
        return BY_NAME.get(name);
    }

}
