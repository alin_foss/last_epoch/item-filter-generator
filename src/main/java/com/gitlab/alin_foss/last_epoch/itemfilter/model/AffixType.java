package com.gitlab.alin_foss.last_epoch.itemfilter.model;

public enum AffixType {
    PREFIX,
    SUFFIX
}
