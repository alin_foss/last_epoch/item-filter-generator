package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum RuleType {
    ERROR("ERROR")

    ,HIDE("HIDE")
    ,SHOW("SHOW")
    ,RECOLOR("HIGHLIGHT")
    ;

    @XmlValue
    public final String name;

    private static final Map<String, RuleType> BY_NAME = new HashMap<>();

    static {
        for (RuleType fi: values()) {
            BY_NAME.put(fi.name, fi);
        }
    }

    public static RuleType getFromName(String name) {
        return BY_NAME.get(name);
    }

}
