package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.*;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum EquipmentSubType {
    // I have a feeling that they don't have "hardcoded" IDs per subtype, rather the ID of the subtype is the "previous ID" plus 1.
    // This way if they add a weapon somewhere in the middle they keep their ordering and only change two values instead of having to change the IDs of every subtype that follows
    // #foreshadowing

    ERROR(EquipmentType.ERROR, true,-1)

    ,HATCHET(EquipmentType.ONE_HANDED_AXE, false, 0)
    ,PICKAXE(EquipmentType.ONE_HANDED_AXE, false, 1)
    ,RUSTED_CLEAVER(EquipmentType.ONE_HANDED_AXE, false, 2)
    ,BATTLE_AXE(EquipmentType.ONE_HANDED_AXE, false, 3)
    ,SICKLE(EquipmentType.ONE_HANDED_AXE, false, 4)
    ,WENGARI_AXE(EquipmentType.ONE_HANDED_AXE, false, 5)
    ,CRESCENT_AXE(EquipmentType.ONE_HANDED_AXE, false, 6)
    ,CHITIN_CLAW(EquipmentType.ONE_HANDED_AXE, false, 7)
    ,EAGLE_WING(EquipmentType.ONE_HANDED_AXE, false, 8)
    ,SOUL_HARVERSTER(EquipmentType.ONE_HANDED_AXE, false, 9)
    ,SHADOW_CLEAVER(EquipmentType.ONE_HANDED_AXE, false, 10)
    ,DRAGONBONE_AXE(EquipmentType.ONE_HANDED_AXE, false, 11)
    ,CONSTELLATION_CLEAVER(EquipmentType.ONE_HANDED_AXE, true, 12)
    ,RESPENDENT_AXE(EquipmentType.ONE_HANDED_AXE, true, 13)


    ,CLUB(EquipmentType.ONE_HANDED_MACES, false, 0)
    ,IRON_HAMMER(EquipmentType.ONE_HANDED_MACES, false, 1)
    ,FLANGED_MACE(EquipmentType.ONE_HANDED_MACES, false, 2)
    ,MORNING_STAR(EquipmentType.ONE_HANDED_MACES, false, 3)
    ,PICK_HAMMER(EquipmentType.ONE_HANDED_MACES, false, 4)
    ,EBERTUSK_CLUB(EquipmentType.ONE_HANDED_MACES, false, 5)
    ,URCHIN_STAR(EquipmentType.ONE_HANDED_MACES, false, 6)
    ,SERPENT_MACE(EquipmentType.ONE_HANDED_MACES, false, 7)
    ,SOLARUM_HAMMER(EquipmentType.ONE_HANDED_MACES, false, 8)
    ,DOOM_STAR(EquipmentType.ONE_HANDED_MACES, false, 9)
    ,RUNE_HAMMER(EquipmentType.ONE_HANDED_MACES, false, 10)
    ,TRADE_STAR(EquipmentType.ONE_HANDED_MACES, true, 11)
    ,PLANETARY_MACE(EquipmentType.ONE_HANDED_MACES, true, 12)

    ,BRASS_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 0)
    ,CULTIST_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 1)
    ,OAK_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 2)
    ,ORNATE_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 3)
    ,ICE_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 4)
    ,AURIC_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 5)
    ,ARGENT_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 6)
    ,SKELTAL_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 7)
    ,OBSIDIAN_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, false, 8)
    ,CELESTIAL_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, true, 9)
    ,VAULT_SCEPTRE(EquipmentType.ONE_HANDED_SCEPTRE, true, 10)

    // Fuck it, JAXB won't give me context info when parsing anyway.
    // I won't be able to convert the int to the proper enum without knowing the value of the EquipmentType tag before it, and I'm not gonna switch to DOM or SAX for this.
    // It's 2024, bad xml is bad xml. I'm not gonna make boilerplate code because of bad xml.

    ;


    @XmlTransient
    public final EquipmentType superType;

    @XmlTransient
    public final boolean isUnique;

    @XmlValue
    public final Integer id;


    private static final Map<EquipmentType, List<EquipmentSubType>> BY_SUPERTYPE = new EnumMap<>(EquipmentType.class);
    private static final Map<EquipmentType, Map<Integer, EquipmentSubType>> BY_SUPERTYPE_AND_ID = new EnumMap<>(EquipmentType.class);

    static {
        for (EquipmentSubType est: values()) {
            BY_SUPERTYPE_AND_ID.computeIfAbsent(est.superType, x -> new HashMap<>())
                    .put(est.id, est);
            BY_SUPERTYPE.computeIfAbsent(est.superType, x -> new ArrayList<>())
                    .add(est);
        }
    }

    public static EquipmentSubType getFromSupertypeAndID(EquipmentType type, Integer id) {
        return BY_SUPERTYPE_AND_ID.get(type).get(id);
    }

    public static List<EquipmentSubType> of(EquipmentType type) {
        return List.copyOf(BY_SUPERTYPE.get(type));
    }

}
