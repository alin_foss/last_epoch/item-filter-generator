package com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.adapters;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.FilterIconColor;
import jakarta.xml.bind.annotation.adapters.XmlAdapter;

public class FilterIconColorAdapter extends XmlAdapter<Integer, FilterIconColor> {

    @Override
    public FilterIconColor unmarshal(Integer iconColorID) {
        return FilterIconColor.getFromCode(iconColorID);
    }

    @Override
    public Integer marshal(FilterIconColor filterIconColor) {
        return filterIconColor.intCode;
    }

}
