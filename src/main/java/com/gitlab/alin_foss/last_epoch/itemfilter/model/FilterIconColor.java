package com.gitlab.alin_foss.last_epoch.itemfilter.model;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@XmlAccessorType(XmlAccessType.FIELD)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum FilterIconColor {
    ERROR(-1)
    ,LIGHT_GRAY(0)  ,WHITE(1)           ,LIME(2)    ,LIGHT_BROWN(3) ,PEACH(4)   ,LIGHT_RED(5)
    ,LIGHT_PINK(6)  ,LIGHT_PURPLE(7)    ,SKY(8)     ,CYAN(9)        ,GREEN(10)  ,YELLOW(11)
    ,ORANGE(12)     ,RED(13)            ,PINK(14)   ,PURPLE(15)     ,BLUE(16)   ,AQUA(17)
    ;

    @XmlValue
    public final Integer intCode;

    private static final Map<Integer, FilterIconColor> BY_INTCODE = new HashMap<>();

    static {
        for (FilterIconColor fi: values()) {
            BY_INTCODE.put(fi.intCode, fi);
        }
    }

    public static FilterIconColor getFromCode(Integer intCode) {
        return BY_INTCODE.get(intCode);
    }

}
