package com.gitlab.alin_foss.last_epoch.generator;

import com.gitlab.alin_foss.last_epoch.db.Affixes;
import com.gitlab.alin_foss.last_epoch.itemfilter.model.*;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class ItemFilterGenerator {

    public static List<ItemFilter> affixBreakdownPerEquipmentTypePerAffixType(String labelsString) {
        Map<Integer, Affix> exclusiveEquipmentAffixes = Affixes.getAffixesByExclusivity().get(1);

        Set<Affix> allIdolPrefixes = new HashSet<>();
        Set<Affix> allIdolSuffixes = new HashSet<>();
        List<EquipmentType> allIdols = new ArrayList<>();

        ItemFilter equipmentSubtypesFilter = new ItemFilter()
                .setName("Equipment subtypes breakdown")
                .setFilterIcon(FilterIcon.TOTEMS)
                .setFilterIconColor(FilterIconColor.ORANGE)
                .setDescription("")
                ;

        ItemFilter weaponsAffixFilter = new ItemFilter()
                .setName("Weapon affix breakdown")
                .setFilterIcon(FilterIcon.SWORD)
                .setFilterIconColor(FilterIconColor.ORANGE)
                .setDescription("")
                ;

        ItemFilter equipmentAffixFilter = new ItemFilter()
                .setName("Equipment affix breakdown")
                .setFilterIcon(FilterIcon.ARMOR)
                .setFilterIconColor(FilterIconColor.ORANGE)
                .setDescription("")
                ;


        // Add equipment affixes. Exclude idol affixes.
        for(EquipmentType et : EquipmentType.values()) {
            if(et.equals(EquipmentType.ERROR)) continue;


            if(Boolean.TRUE.equals(et.isIdol)) {
                allIdols.add(et);
                allIdolPrefixes.addAll(Affixes.getAffixBreakdown().get(et).get(AffixType.PREFIX));
                allIdolSuffixes.addAll(Affixes.getAffixBreakdown().get(et).get(AffixType.SUFFIX));
                continue;
            }

            equipmentSubtypesFilter.addRule(new Rule()
                    .setType(RuleType.SHOW)
                    .setNameOverride(String.join(", ",et.name + " subtypes" + " // " +
                            "SLOT:" + et.name,
                            "CND:SUBTYPE",
                            "IS_PREFIX:NO",
                            "IS_SUFFIX:NO",
                            "IS_IDOL:NO",
                            "IS_EQUIP:YES",
                            "EQUIP:" + (et.isWeapon ? "WEAPON" : "ARMOR"),
                            labelsString
                    ))
                    .addCondition(Condition.newSubtypeCondition(List.of(et), Collections.emptyList()))
            );

            ItemFilter itemFilter = equipmentAffixFilter;
            if(et.isWeapon) itemFilter = weaponsAffixFilter;

            Set<Affix> allEquipmentPrefixes = Affixes.getAffixBreakdown().get(et).get(AffixType.PREFIX);
            Set<Affix> allEquipmentSuffixes = Affixes.getAffixBreakdown().get(et).get(AffixType.SUFFIX);

            Set<Affix> exclusiveEquipmentPrefixes = allEquipmentPrefixes.stream()
                    .filter(x -> exclusiveEquipmentAffixes.containsKey(x.getAffixId()))
                    .collect(Collectors.toSet());

            Set<Affix> exclusiveEquipmentSuffixes = allEquipmentSuffixes.stream()
                    .filter(x -> exclusiveEquipmentAffixes.containsKey(x.getAffixId()))
                    .collect(Collectors.toSet());

            Condition allEquipmentPrefixesCondition = Condition.newAffixCondition(new ArrayList<>(allEquipmentPrefixes), 1);
            Condition allEquipmentSuffixesCondition = Condition.newAffixCondition(new ArrayList<>(allEquipmentSuffixes), 1);
            Condition exclusiveEquipmentPrefixesCondition = Condition.newAffixCondition(new ArrayList<>(exclusiveEquipmentPrefixes), 1);
            Condition exclusiveEquipmentSuffixesCondition = Condition.newAffixCondition(new ArrayList<>(exclusiveEquipmentSuffixes), 1);

            if(!exclusiveEquipmentSuffixesCondition.getAffixes().isEmpty()) {
                itemFilter.addRule(new Rule()
                        .setType(RuleType.SHOW)
                        .setNameOverride(String.join(", ", et.name + " Exclusive suffixes // " +
                                        "SLOT:" + et.name,
                                "CND:AFFIX",
                                "AFFIX:SUFFIX",
                                "IS_PREFIX:NO",
                                "IS_SUFFIX:YES",
                                "IS_IDOL:NO",
                                "IS_EQUIP:YES",
                                "EQUIP:" + (et.isWeapon ? "WEAPON" : "ARMOR"),
                                "IS_EXCL:YES",
                                labelsString
                        ))
                        .addCondition(exclusiveEquipmentSuffixesCondition)
                );
            }
            if(!exclusiveEquipmentPrefixesCondition.getAffixes().isEmpty()) {
                itemFilter.addRule(new Rule()
                        .setType(RuleType.SHOW)
                        .setNameOverride(String.join(", ", et.name + " Exclusive prefixes // " +
                                        "SLOT:" + et.name,
                                "CND:AFFIX",
                                "AFFIX:PREFIX",
                                "IS_PREFIX:YES",
                                "IS_SUFFIX:NO",
                                "IS_IDOL:NO",
                                "IS_EQUIP:YES",
                                "EQUIP:" + (et.isWeapon ? "WEAPON" : "ARMOR"),
                                "IS_EXCL:YES",
                                labelsString
                        ))
                        .addCondition(exclusiveEquipmentPrefixesCondition)
                );
            }
            itemFilter.addRule(new Rule()
                    .setType(RuleType.SHOW)
                    .setNameOverride(String.join(", ",et.name + " suffixes // " +
                            "SLOT:" + et.name,
                            "CND:AFFIX",
                            "AFFIX:SUFFIX",
                            "IS_PREFIX:NO",
                            "IS_SUFFIX:YES",
                            "IS_IDOL:NO",
                            "IS_EQUIP:YES",
                            "EQUIP:" + (et.isWeapon ? "WEAPON" : "ARMOR"),
                            labelsString
                    ))
                    .addCondition(allEquipmentSuffixesCondition)
            );
            itemFilter.addRule(new Rule()
                    .setType(RuleType.SHOW)
                    .setNameOverride(String.join(", ",et.name + " prefixes // " +
                            "SLOT:" + et.name,
                            "CND:AFFIX",
                            "AFFIX:PREFIX",
                            "IS_PREFIX:YES",
                            "IS_SUFFIX:NO",
                            "IS_IDOL:NO",
                            "IS_EQUIP:YES",
                            "EQUIP:" + (et.isWeapon ? "WEAPON" : "ARMOR"),
                            "IS_EXCL:YES",
                            labelsString
                    ))
                    .addCondition(allEquipmentPrefixesCondition)
            );
        }

        Condition allIdolsCondition = Condition.newSubtypeCondition(allIdols, Collections.emptyList());
        Condition allIdolPrefixesCondition = Condition.newAffixCondition(new ArrayList<>(allIdolPrefixes), 1);
        Condition allIdolSuffixesCondition = Condition.newAffixCondition(new ArrayList<>(allIdolSuffixes), 1);

        equipmentAffixFilter.addRule(new Rule()
                .setType(RuleType.SHOW)
                .setNameOverride(String.join(" ,","All idol suffixes // " +
                        "SLOT:IDOL",
                        "CND:AFFIX",
                        "AFFIX:SUFFIX",
                        "IS_PREFIX:NO",
                        "IS_SUFFIX:YES",
                        "IS_IDOL:YES",
                        "IS_EQUIP:NO",
                        "IS_EXCL:NO",
                        labelsString
                ))
                .addCondition(allIdolSuffixesCondition)
        );

        equipmentAffixFilter.addRule(new Rule()
                .setType(RuleType.SHOW)
                .setNameOverride(String.join(" ,","All idol prefixes // " +
                        "SLOT:IDOL",
                        "CND:AFFIX",
                        "AFFIX:PREFIX",
                        "IS_PREFIX:YES",
                        "IS_SUFFIX:NO",
                        "IS_IDOL:YES",
                        "IS_EQUIP:NO",
                        "IS_EXCL:NO",
                        labelsString
                ))
                .addCondition(allIdolPrefixesCondition)
        );

        equipmentSubtypesFilter.addRule(new Rule()
                .setType(RuleType.SHOW)
                .setNameOverride(String.join(" ,","IDOL subtypes // " +
                        "SLOT:IDOL",
                        "CND:SUBTYPE",
                        "IS_PREFIX:NO",
                        "IS_SUFFIX:NO",
                        "IS_IDOL:YES",
                        "IS_EQUIP:NO",
                        labelsString
                ))
                .addCondition(allIdolsCondition)
        );

        return List.of(equipmentSubtypesFilter, weaponsAffixFilter, equipmentAffixFilter);
    }

    // TODO add a lot of unit tests until there's 100% code coverage AND you covered all stupid rule names
    public static ItemFilter process(ItemFilter template, List<ItemFilter> inputs) {
        ItemFilter output = new ItemFilter(template);
        output.clearRules();

        Map<String, Map<String, Set<Rule>>> labelValuesToRules = new HashMap<>();
        Map<Rule, Map<String, String>> rulesToLabels = new HashMap<>();

        for(ItemFilter filter : inputs) {
            for(Rule rule : filter.getRules()) {
                if(!rule.getIsEnabled()) {
                    continue;
                }
                String[] split = rule.getNameOverride().split("//");
                if(split.length != 2) {
                    log.warn("Filter '{}' contains rule without labels: '{}'", filter.getName(), rule.getNameOverride());
                    continue;
                }

                if(split[1].trim().isEmpty()) {
                    log.warn("Filter '{}' contains rule with empty labels list: '{}'", filter.getName(), rule.getNameOverride());
                    continue;
                }

                for(String labelKV : split[1].split(",")) {
                    String[] kv = labelKV.split(":");
                    if(kv.length != 2) {
                        log.warn("Filter '{}' contains rule '{}' with confusing label: '{}'", filter.getName(), rule.getNameOverride(), labelKV);
                        continue;
                    }

                    String key = kv[0].trim().toLowerCase();
                    String value = kv[1].trim().toLowerCase();
                    labelValuesToRules
                            .computeIfAbsent(key.trim(), k -> new HashMap<>())
                            .computeIfAbsent(value.trim(), v -> new HashSet<>())
                            .add(rule);
                    rulesToLabels
                            .computeIfAbsent(rule, k -> new HashMap<>())
                            .put(key, value);
                }
            }
        }

        for(Rule rule : template.getRules()) {
            output.addAllRules(processRule(rule, labelValuesToRules, rulesToLabels));
        }

        return output;
    }

    // TODO add a lot of unit tests until there's 100% code coverage AND you covered all stupid rule names
    private static List<Rule> processRule(Rule templateRule,
            Map<String, Map<String, Set<Rule>>> rulesByLabel,
            Map<Rule, Map<String, String>> rulesToLabels
    ) {
        log.info("Processing for template rule '{}'", templateRule.getNameOverride());
        if(Boolean.FALSE.equals(templateRule.getIsEnabled())) {
            log.info("Skipping this disabled rule.");
            return List.of();
        }

        // If rule does not require processing, just copy it
        if(!templateRule.getNameOverride().contains("//")) {
            log.info("No instructions. Returning the rule itself.");
            return List.of(templateRule);
        }

        // Select the "programming" instructions
        String[] split = templateRule.getNameOverride().split("//");
        if(split.length != 2) {
            log.warn("Template filter has rule with confusing name: '{}'", templateRule.getNameOverride());
            log.info("Returning the rule itself.");
            return List.of(templateRule);
        }
        String outputRuleName = split[0];
        String instructionsRaw = split[1];

        // Separate the instructions. Currently just SELECT and JOIN_BY
        String[] instructions = instructionsRaw.split(";");
        if(!instructions[0].trim().toLowerCase().startsWith("select")) {
            log.warn("The first instruction should always be select. This rule in the template is wrong: {}", templateRule.getNameOverride());
            return List.of(templateRule);
        }

        Set<Rule> selectedRules = new HashSet<>(); // Rules that match the queried label key/value pairs in the SELECT clause
        String[] selectionLabelsKV = instructions[0].trim().substring("select".length()).trim().split(",");
        for(String selectionLabelKV : selectionLabelsKV) {
            String[] kv = selectionLabelKV.split(":");
            if(kv.length != 2) {
                log.warn("Template filter contains rule '{}' with confusing label: '{}'", templateRule.getNameOverride(), selectionLabelKV);
                return List.of(templateRule);
            }

            String key = kv[0].trim().toLowerCase();
            String value = kv[1].trim().toLowerCase();

            // The first time, select all the rules that have the first sought-after label.
            if(selectedRules.isEmpty()) {
                selectedRules.addAll(rulesByLabel.get(key).getOrDefault(value,Set.of()));
            } else {
                Set<Rule> toRemove = new HashSet<>();
                // After having an initial set of rules, remove all rules that don't contain the additionally requested labels.
                for(Rule r : selectedRules) {
                    Map<String, String> ruleLabels = rulesToLabels.get(r);
                    if( ! (ruleLabels.containsKey(key) && ruleLabels.get(key).equals(value)) ) {
                        toRemove.add(r);
                    }
                }
                selectedRules.removeAll(toRemove);
            }
        }

        if(instructions.length > 1 && instructions[1].trim().toLowerCase().startsWith("join_by")) {
            List<String> joinByLabelKeys = Arrays.stream(instructions[1].trim().substring("join_by".length()).split(","))
                    .map(String::trim)
                    .map(String::toLowerCase)
                    .toList();
            Map<String, Rule> grouping = new HashMap<>();
            for(Rule r : selectedRules) {
                Map<String, String> thisRulesJoinByLabelKVs = joinByLabelKeys.stream().collect(Collectors.toMap(
                        lk -> lk,
                        lk -> rulesToLabels.get(r).get(lk)
                ));
                String groupingKey = joinByLabelKeys.stream()
                        .map(thisRulesJoinByLabelKVs::get)
                        .collect(Collectors.joining(","));

                if(grouping.containsKey(groupingKey)) {
                    grouping.put(groupingKey, joinRules(grouping.get(groupingKey), r));
                } else {
                    // Set the description: override wildcards
                    String ruleName = outputRuleName;
                    for(var entry : thisRulesJoinByLabelKVs.entrySet()) {
                        ruleName = ruleName.toLowerCase().replaceAll("\\$" + entry.getKey(), entry.getValue());
                    }
                    templateRule.setNameOverride(ruleName);
                    grouping.put(groupingKey, joinRules(templateRule, r));
                }
            }
            selectedRules = new HashSet<>(grouping.values());
        }

        return new ArrayList<>(selectedRules);
    }

    private static Rule joinRules(Rule baseRule, Rule overridingRule) {
        Rule result = new Rule(baseRule);
        Condition originalAffixCondition = null;
        Condition originalSubtypeCondition = null;
        Condition originalRarityCondition = null;
        for(Condition c : result.getConditions()) {
            switch (c.getItype()) {
                case AFFIX: originalAffixCondition = c; break;
                case SUBTYPE: originalSubtypeCondition = c; break;
                case RARITY: originalRarityCondition = c; break;
                default: break;
            }
        }

        for(Condition c : overridingRule.getConditions()) {
            switch (c.getItype()) {
                case AFFIX:
                    if(originalAffixCondition == null) {
                        originalAffixCondition = Condition.newAffixCondition(List.of(), 0);
                        result.addCondition(originalAffixCondition);
                    }
                    originalAffixCondition.getAffixes().addAll(c.getAffixes());
                    break;
                case SUBTYPE:
                    if(originalSubtypeCondition == null) {
                        originalSubtypeCondition = Condition.newSubtypeCondition(new ArrayList<>(), new ArrayList<>());
                        result.addCondition(originalSubtypeCondition);
                    }
                    // TODO maybe do all the checks and warn for "incompatible" joins, but due to Last Epoch logic this won't break anything anyways.
                    originalSubtypeCondition.addAllTypes(c.getType());
                    originalSubtypeCondition.addAllSubtypes(c.getSubTypes());
                    break;
                case RARITY:
                    if(originalRarityCondition == null) {
                        originalRarityCondition = Condition.newRarityCondition(false,false,false,false,false,false);
                        result.addCondition(originalRarityCondition);
                    }
                    originalRarityCondition.setRarity(Condition.newRarityCondition(
                            originalRarityCondition.getRarity().contains("NORMAL")  || c.getRarity().contains("NORMAL"),
                            originalRarityCondition.getRarity().contains("MAGIC")   || c.getRarity().contains("MAGIC"),
                            originalRarityCondition.getRarity().contains("RARE")    || c.getRarity().contains("RARE"),
                            originalRarityCondition.getRarity().contains("UNIQUE")  || c.getRarity().contains("UNIQUE"),
                            originalRarityCondition.getRarity().contains("SET")     || c.getRarity().contains("SET"),
                            originalRarityCondition.getRarity().contains("EXALTED") || c.getRarity().contains("EXALTED")
                    ).getRarity());
                    break;
                default: break;
            }
        }
        return result;
    }

}
