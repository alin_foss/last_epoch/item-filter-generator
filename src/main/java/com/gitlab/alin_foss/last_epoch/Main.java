package com.gitlab.alin_foss.last_epoch;

import com.gitlab.alin_foss.last_epoch.db.frominput.lastepochtools.InputAffixesFromLastepochtoolscom;
import com.gitlab.alin_foss.last_epoch.db.frominput.lastepochtools.RetrieveAffixes;
import com.gitlab.alin_foss.last_epoch.generator.ItemFilterGenerator;
import com.gitlab.alin_foss.last_epoch.itemfilter.marshaller.ItemFilterMarshaller;
import com.gitlab.alin_foss.last_epoch.itemfilter.model.ItemFilter;
import jakarta.xml.bind.JAXBException;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import javax.script.ScriptException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.gitlab.alin_foss.last_epoch.itemfilter.util.Util.getResourceFile;

@Log4j2
public class Main {

    @SneakyThrows
    private static void testMarshaller(ItemFilterMarshaller ifMarshaller) {
        File inputFile = getResourceFile("inputXml/BigMageFilter.xml");
        ItemFilter itemFilter = ifMarshaller.unmarshall(inputFile);

        File outputFile = new File("output/marshalledFilter.xml");
        ifMarshaller.marshall(outputFile, itemFilter);
    }
    public static void main(String[] args) throws JAXBException, IOException, TransformerException, ScriptException, InterruptedException {
        log.info("Hello, world!");
        RetrieveAffixes.retrieveAffixes(true);
        new InputAffixesFromLastepochtoolscom();

        ItemFilterMarshaller ifMarshaller = new ItemFilterMarshaller();
        // testMarshaller(ifMarshaller);

//        List<ItemFilter> itemFilters = ItemFilterGenerator.affixBreakdownPerEquipmentTypePerAffixType("SPEC:BM, BUILD:BOBBER");
//        ifMarshaller.marshall(new File("output/__IN_Implicite.xml"), itemFilters.get(0));
//        ifMarshaller.marshall(new File("output/__IN_Afix_arma.xml"), itemFilters.get(1));
//        ifMarshaller.marshall(new File("output/__IN_Afix_armura.xml"), itemFilters.get(2));

        // Test generator
        ItemFilter templateFilter = ifMarshaller.unmarshall(getResourceFile("test_input/__TEMPLATE.xml"));
        ItemFilter generatedFilter = ItemFilterGenerator.process(templateFilter, List.of(
                ifMarshaller.unmarshall(getResourceFile("test_input/__IN_Afix_arma.xml")),
                ifMarshaller.unmarshall(getResourceFile("test_input/__IN_Afix_armura.xml")),
                ifMarshaller.unmarshall(getResourceFile("test_input/__IN_Implicite.xml"))
        ));
        ifMarshaller.marshall(new File("output/__GENERATED.xml"), generatedFilter);

        log.info("Goodbye, world!");
    }
}
