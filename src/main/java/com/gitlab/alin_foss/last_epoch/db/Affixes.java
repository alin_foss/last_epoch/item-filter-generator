package com.gitlab.alin_foss.last_epoch.db;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.Affix;
import com.gitlab.alin_foss.last_epoch.itemfilter.model.AffixType;
import com.gitlab.alin_foss.last_epoch.itemfilter.model.EquipmentType;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Affixes {

    private static final Map<Integer, Affix> BY_ID = new HashMap<>();
    private static final Map<Integer, Map<Integer, Affix>> BY_EXCLUSIVITY = new HashMap<>();
    private static final Map<EquipmentType, Map<AffixType, Set<Affix>>> BY_EQUIPMENT_AND_AFFIX_TYPES = new EnumMap<>(EquipmentType.class);

    public static Affix getFromId(Integer affixId) {
        return BY_ID.get(affixId);
    }

    public static Collection<Affix> getAllAffixes() {
        return Collections.unmodifiableCollection(BY_ID.values());
    }

    public static  Map<Integer, Map<Integer, Affix>> getAffixesByExclusivity() {
        return Collections.unmodifiableMap(BY_EXCLUSIVITY);
    }

    public static Map<EquipmentType, Map<AffixType, Set<Affix>>> getAffixBreakdown() {
        // This ain't gonna do shit. Okay you can't modify top level maps but you can modify the maps inside the map.
        return Collections.unmodifiableMap(BY_EQUIPMENT_AND_AFFIX_TYPES);
    }

    public static void registerAffix(Affix affix) {
        BY_ID.put(affix.getAffixId(), affix);

        Integer exclusivity = affix.getCanRollOn().size();
        Map<Integer, Affix> exclusivityAffixes = BY_EXCLUSIVITY.computeIfAbsent(exclusivity, k -> new HashMap<Integer, Affix>());
        exclusivityAffixes.put(affix.getAffixId(), affix);

        for(EquipmentType et : affix.getCanRollOn()) {
            Map<AffixType, Set<Affix>> perEquipmentPerAffixType = BY_EQUIPMENT_AND_AFFIX_TYPES.computeIfAbsent(et, k -> new EnumMap<>(AffixType.class));
            Set<Affix> affixes = perEquipmentPerAffixType.computeIfAbsent(affix.getType(), k -> new HashSet<>());
            affixes.add(affix);
        }
    }

}
