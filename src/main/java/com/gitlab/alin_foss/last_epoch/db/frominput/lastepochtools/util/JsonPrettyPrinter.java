package com.gitlab.alin_foss.last_epoch.db.frominput.lastepochtools.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;

import java.io.IOException;

public class JsonPrettyPrinter extends DefaultPrettyPrinter {
    public JsonPrettyPrinter() {
        DefaultPrettyPrinter.Indenter indenter = new DefaultIndenter("  ", System.lineSeparator());
        indentObjectsWith(indenter);
        indentArraysWith(indenter);
        _objectFieldValueSeparatorWithSpaces = ": ";
    }

    private JsonPrettyPrinter(JsonPrettyPrinter pp) {
        super(pp);
    }

    @Override
    public void writeEndArray(JsonGenerator g, int nrOfValues) throws IOException {
        if (!_arrayIndenter.isInline()) {
            --_nesting;
        }
        if (nrOfValues > 0) {
            _arrayIndenter.writeIndentation(g, _nesting);
        }
        g.writeRaw(']');
    }

    @Override
    public DefaultPrettyPrinter createInstance() {
        return new JsonPrettyPrinter(this);
    }
}
