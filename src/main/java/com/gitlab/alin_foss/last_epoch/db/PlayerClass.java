package com.gitlab.alin_foss.last_epoch.db;

public enum PlayerClass {
    ANY,
    SENTINEL,
    ACOLYTE,
    PRIMALIST,
    ROGUE,
    MAGE
}
