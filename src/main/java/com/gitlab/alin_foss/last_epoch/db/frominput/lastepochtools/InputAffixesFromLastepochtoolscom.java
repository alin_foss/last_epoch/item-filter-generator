package com.gitlab.alin_foss.last_epoch.db.frominput.lastepochtools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.alin_foss.last_epoch.itemfilter.model.Affix;
import com.gitlab.alin_foss.last_epoch.itemfilter.model.EquipmentType;
import com.gitlab.alin_foss.last_epoch.itemfilter.util.Util;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Log4j2
public class InputAffixesFromLastepochtoolscom {

    private static final String AFFIX_DEFINITIONS_PATH = String.format("%s/affixdb/lastepochtools/affixList-formatted.json", System.getProperty("user.dir")).replace("\\", "/");
    private static final String TOOLTIP_DEFINITIONS_PATH = String.format("%s/affixdb/lastepochtools/tooltip-keys-formatted.json", System.getProperty("user.dir")).replace("\\", "/");
    public InputAffixesFromLastepochtoolscom() throws IOException {
        File affixList = new File(AFFIX_DEFINITIONS_PATH);
        File tooltipKeys = new File(TOOLTIP_DEFINITIONS_PATH);
        ObjectMapper objectMapper = new ObjectMapper();

        @SuppressWarnings("unchecked")
        Map<String, Object> tooltipMap = objectMapper.readValue(tooltipKeys, Map.class);

        @SuppressWarnings("unchecked")
        Map<String, Object> affixMap = objectMapper.readValue(affixList, Map.class);

        @SuppressWarnings("unchecked")
        Map<String, Map<String, Object>> affixesMap = ((Map<String, Map<String, Object>>) affixMap.get("singleAffixes"));

        @SuppressWarnings("unchecked")
        Map<String, Map<String, Object>> multiAffixes =  ((Map<String, Map<String, Object>>) affixMap.get("multiAffixes"));
        affixesMap.putAll(multiAffixes);

        affixesMap.forEach( (key, affix) -> {
            @SuppressWarnings("unchecked")
            List<EquipmentType> canRollOn = ((List<Integer>) affix.get("canRollOn")).stream()
                    .map(Converter::typeToEquipmentType)
                    .toList();

            new Affix(
                    (Integer) (affix.get("affixId")),
                    Converter.typeToAffixType((Integer) affix.get("type")),
                    Converter.rerolLChanceToDouble(affix.get("rerollChance")),
                    canRollOn
            );
        });

        log.info("Loaded lastepoch db json files into memory.");
    }
}
