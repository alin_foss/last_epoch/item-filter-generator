package com.gitlab.alin_foss.last_epoch.db.frominput.lastepochtools;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gitlab.alin_foss.last_epoch.db.frominput.lastepochtools.util.JsonPrettyPrinter;
import com.gitlab.alin_foss.last_epoch.db.frominput.lastepochtools.util.LZString;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.htmlunit.BrowserVersion;
import org.htmlunit.NicelyResynchronizingAjaxController;
import org.htmlunit.WebClient;
import org.htmlunit.html.HtmlPage;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;

@Log4j2
public class RetrieveAffixes {

    private static final String AFFIXES_URL = "https://www.lastepochtools.com/data/version100/i18n/full/en.json";
    private static final String JS_DB_URL = "https://www.lastepochtools.com/db/prefixes";

    private static final String JS_AFFIXES_COMMAND = "JSON.stringify(window.itemDB.affixList);";

    private static final ObjectMapper JSON_FORMATTING_MAPPER = JsonMapper.builder().enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES).build();
    private static final DefaultPrettyPrinter PRETTY_PRINTER = new JsonPrettyPrinter();

    private static final String AFFIX_DEFINITIONS_PATH = String.format("%s/affixdb/lastepochtools/affixList-formatted.json", System.getProperty("user.dir")).replace("\\", "/");
    private static final String TOOLTIP_DEFINITIONS_PATH = String.format("%s/affixdb/lastepochtools/tooltip-keys-formatted.json", System.getProperty("user.dir")).replace("\\", "/");

    public static void retrieveAffixes(boolean force) throws IOException, InterruptedException {
        log.info("Updating Affixes definitions. Force: {}", force);

        //Create affix and tooltip files if they do not exist
        File tooltipDefinitionsFile = new File(TOOLTIP_DEFINITIONS_PATH);
        File affixDefinitionsFile = new File(AFFIX_DEFINITIONS_PATH);

        //If they are created already do nothing, unless forced
        //TODO: check if files are empty
        if (((affixDefinitionsFile.exists() || affixDefinitionsFile.getParentFile().mkdirs()) && (!tooltipDefinitionsFile.exists() || tooltipDefinitionsFile.getParentFile().mkdirs())) || force) {
            setupObjectMapper();

            try (
                    WebClient webClient = new WebClient(BrowserVersion.FIREFOX);
                    BufferedInputStream bis = new BufferedInputStream(URI.create(AFFIXES_URL).toURL().openStream());
                    BufferedOutputStream bosTooltip = new BufferedOutputStream(new FileOutputStream(tooltipDefinitionsFile, false));
                    BufferedOutputStream bosAffix = new BufferedOutputStream(new FileOutputStream(affixDefinitionsFile, false))
            ) {
                log.info("Loading Tooltips from lastepochtools.com");
                ObjectNode tooltipDataParsed = (ObjectNode) JSON_FORMATTING_MAPPER.readTree(bis.readAllBytes());

                log.info("Loading Affixes from lastepochtools.com");
                setupWebClientForJSExecution(webClient);

                log.debug("Opening {}", JS_DB_URL);
                HtmlPage page = webClient.getPage(JS_DB_URL);

                log.debug("Parsing Affixes");
                byte[] affixesList = ((String) page.executeJavaScript(JS_AFFIXES_COMMAND).getJavaScriptResult()).getBytes(StandardCharsets.UTF_8);

                //Parse json
                ObjectNode affixDataParsed = (ObjectNode) JSON_FORMATTING_MAPPER.readTree(affixesList);

                //Update missing fields
                affixDataParsed.get("singleAffixes").forEach(node -> {
                    log.debug("Parsing Single Affix {}", node.get("affixId"));
                    updateNode((ObjectNode) node, tooltipDataParsed, true);
                });
                affixDataParsed.get("multiAffixes").forEach(node -> {
                    log.debug("Parsing Multi Affix {}", node.get("affixId"));
                    updateNode((ObjectNode) node, tooltipDataParsed, false);
                });

                //Write to files
                writeWithFormatterToOutput(bosTooltip, tooltipDataParsed);
                writeWithFormatterToOutput(bosAffix, affixDataParsed);
            }

            log.info("Affixes definitions updated.");
        } else {
            log.info("No update needed for affixes definitions.");
        }
    }

    private static void updateNode(ObjectNode node, ObjectNode tooltipDataParsed, boolean singleAffix) {
        //'id' is constructed like this: A + LZ base 64 from trailing zeros length of affix id and the actual affix id
        // Ex: for affixId: 0 -> A + LZ64("00" + "0") -> AAwo
        String affixId = String.valueOf(((IntNode) node.get("affixId")).intValue());
        String id = "A" + LZString.compress("0".repeat(Math.max(0, 3 - affixId.length())) + affixId);
        log.debug("Parsed 'id' for affix {}", affixId);

        //'ea' is based on affixDisplayNameKey and affixProperties[0].modDisplayNameKey that can be found in the tooltip list
        String nameKey = null;
        if (!node.get("affixDisplayNameKey").isNull()) {
            nameKey = ((TextNode) node.get("affixDisplayNameKey")).asText();
        } else {
            nameKey = ((TextNode) node.get("affixProperties").get(0).get("modDisplayNameKey")).asText();
        }
        log.debug("Parsed 'name' for affix {}", affixId);

        //'affixTitle' can be one of 6 values and is based on 'classSpecificity'
        // Primalist's - 2
        // Mage's - 4
        // Sentinel's - 8
        // Acolyte's - 16
        // Rogue's - 32
        // or 'specialAffixType'
        // Experimental - 1
        String affixTitle = null;
        if (((IntNode) node.get("titleType")).intValue() == 1 && node.get("affixTitleKey").isNull()) {
            switch (((IntNode) node.get("classSpecificity")).intValue()) {
                case 2 -> affixTitle = "Primalist's";
                case 4 -> affixTitle = "Mage's";
                case 8 -> affixTitle = "Sentinel's";
                case 16 -> affixTitle = "Acolyte's";
                case 32 -> affixTitle = "Rogue's";
            }
        } else if (((IntNode) node.get("specialAffixType")).intValue() == 1) affixTitle = "Experimental";
        log.debug("Parsed 'affixTitle' for affix {}", affixId);

        //affixProperties.useGeneratedName is an extra field that is not needed
        node.get("affixProperties").forEach(affixProps -> {
            ((ObjectNode) affixProps).remove("useGeneratedName");
        });

        //Update json nodes; need to cast them to ObjectNode since JsonNode is immutable and 'put' method is unavailable
        ((ObjectNode) node).put("isSingle", singleAffix).put("rarity", 0).put("id", id);
        if (affixTitle != null) ((ObjectNode) node).put("affixTitle", affixTitle);
        ((ObjectNode) node).put("ea", StringUtils.capitalize(((TextNode) tooltipDataParsed.get(nameKey)).asText()).replace("''", "'"));
        log.debug("Updated affix {}", affixId);
    }

    private static void setupObjectMapper() {
        JSON_FORMATTING_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
        PRETTY_PRINTER.indentArraysWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
    }

    private static void setupWebClientForJSExecution(WebClient webClient) {
        //Web Client Configuration for JS execution
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getOptions().setTimeout(30000);
        webClient.getOptions().setCssEnabled(true);
        webClient.getOptions().setFetchPolyfillEnabled(true);
        webClient.setJavaScriptTimeout(30000);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());
    }

    private static void writeWithFormatterToOutput(BufferedOutputStream boss, ObjectNode data) throws IOException {
        String dataFormatted = JSON_FORMATTING_MAPPER.writer(PRETTY_PRINTER).writeValueAsString(data);
        boss.write(dataFormatted.getBytes(StandardCharsets.UTF_8));
    }
}
