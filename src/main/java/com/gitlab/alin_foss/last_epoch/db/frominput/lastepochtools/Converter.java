package com.gitlab.alin_foss.last_epoch.db.frominput.lastepochtools;

import com.gitlab.alin_foss.last_epoch.itemfilter.model.AffixType;
import com.gitlab.alin_foss.last_epoch.itemfilter.model.EquipmentType;

import java.util.HashMap;
import java.util.Map;

public class Converter {

    static Map<Integer, EquipmentType> typeToEquipmentType = new HashMap<>();
    static {
        typeToEquipmentType.put(0, EquipmentType.HELMET);
        typeToEquipmentType.put(1, EquipmentType.BODY_ARMOR);
        typeToEquipmentType.put(2, EquipmentType.BELT);
        typeToEquipmentType.put(3, EquipmentType.BOOTS);
        typeToEquipmentType.put(4, EquipmentType.GLOVES);
        typeToEquipmentType.put(5, EquipmentType.ONE_HANDED_AXE);
        typeToEquipmentType.put(6, EquipmentType.ONE_HANDED_DAGGER);
        typeToEquipmentType.put(7, EquipmentType.ONE_HANDED_MACES);
        typeToEquipmentType.put(8, EquipmentType.ONE_HANDED_SCEPTRE);
        typeToEquipmentType.put(9, EquipmentType.ONE_HANDED_SWORD);
        typeToEquipmentType.put(10, EquipmentType.WAND);
        typeToEquipmentType.put(12, EquipmentType.TWO_HANDED_AXE);
        typeToEquipmentType.put(13, EquipmentType.TWO_HANDED_MACE);
        typeToEquipmentType.put(14, EquipmentType.TWO_HANDED_SPEAR);
        typeToEquipmentType.put(15, EquipmentType.TWO_HANDED_STAFF);
        typeToEquipmentType.put(16, EquipmentType.TWO_HANDED_SWORD);
        typeToEquipmentType.put(17, EquipmentType.QUIVER);
        typeToEquipmentType.put(18, EquipmentType.SHIELD);
        typeToEquipmentType.put(19, EquipmentType.CATALYST);
        typeToEquipmentType.put(20, EquipmentType.AMULET);
        typeToEquipmentType.put(21, EquipmentType.RING);
        typeToEquipmentType.put(22, EquipmentType.RELIC);
        typeToEquipmentType.put(23, EquipmentType.BOW);
        typeToEquipmentType.put(25, EquipmentType.IDOL_1X1_ETERRA); // Small (Eterrian) Idol
        typeToEquipmentType.put(26, EquipmentType.IDOL_1X1_LAGON); // Small Lagonian Idol
        typeToEquipmentType.put(27, EquipmentType.IDOL_2X1); // Humble Idol
        typeToEquipmentType.put(28, EquipmentType.IDOL_1X2); // Stout Idol
        typeToEquipmentType.put(29, EquipmentType.IDOL_3X1); // Grand Idol
        typeToEquipmentType.put(30, EquipmentType.IDOL_1X3); // Large Idol
        typeToEquipmentType.put(31, EquipmentType.IDOL_4X1); // Ornate Idol
        typeToEquipmentType.put(32, EquipmentType.IDOL_1X4); // Huge Idol
        typeToEquipmentType.put(33, EquipmentType.IDOL_2X2); // Adorned Idol
    }

    public static EquipmentType typeToEquipmentType(Integer type) {
        return typeToEquipmentType.getOrDefault(type, EquipmentType.ERROR);
    }

    public static AffixType typeToAffixType(Integer type) {
        if (type == 0) {
            return AffixType.PREFIX;
        }
        return AffixType.SUFFIX;
    }

    public static Double rerolLChanceToDouble(Object object) {
        if(object instanceof Double asDouble) {
            return asDouble;
        } else if (object instanceof Integer asInt) {
            return Double.valueOf(asInt);
        }
        return -1d;
    }

}
